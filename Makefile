MOPSA_BIN=$(shell dirname $(shell which mopsa))
MOPSA_OPTIONS?=
MOPSA_ARGS=${MOPSA_OPTIONS} -silent
OUTPUT_DIR?=.

ifneq (,$(findstring json,$(MOPSA_ARGS)))
	MOPSA_OUT_EXT=.json
else
	MOPSA_OUT_EXT=.txt
endif


UNITTEST_FILE=tests/llist_test.py
UNITTEST_NAME=$(shell basename -s .py ${UNITTEST_FILE})
UNITTEST_OUT=${OUTPUT_DIR}/${UNITTEST_NAME}${MOPSA_OUT_EXT}


all: build unittests

build:
	mopsa-build python3 setup.py build --force > build_log 2>&1

unittests:
	mkdir -p ${OUTPUT_DIR}
	if [ -f ${UNITTEST_OUT} ]; then mv ${UNITTEST_OUT} ${UNITTEST_OUT}.old; fi;
	mopsa-cpython ${MOPSA_ARGS} ${UNITTEST_FILE} 1>${UNITTEST_OUT} 2>${OUTPUT_DIR}/${UNITTEST_NAME}.stderr

.PHONY: build unittests
