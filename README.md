This repository is a copy of the [llist library](https://github.com/ajakubek/python-llist), using commit e9626bfab3231aa132c8fbfe6ccc92479fa743ec.

Run `make` to build the library using `mopsa-build` and launch the analysis of the tests.
The results are written in text files by default.

Minor changes performed to analyze the library are shown in CHANGELOG.md
